package ru.eka.identif;

import java.util.Scanner;

/**
 * Класс который обнуляет столбец прямоугольной матрицы,
 * который соответствует заданному числу.
 *
 * @author Куцкая Э.А., 15ОИТ18.
 */

public class Delete {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[][] mas = new int[5][7];
        filling(mas);
        write(mas);
        System.out.println("Какой столбик не нужен? ");
        int chislo = scanner.nextInt();
        zeroing(mas, chislo);

    }

    /**
     * Метод для вывода матрицы
     *
     * @param mas матрица
     */
    private static void write(int[][] mas) {
        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas[i].length; j++) {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }
    }

    /**
     * Метод для обнуления столбца в матрице
     *
     * @param mas матрица
     * @param chislo обнуляемый столбец
     */
    private static void zeroing(int[][] mas, int chislo) {
        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas[i].length; j++) {
                mas[i][chislo - 1] = 0;
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }
    }

    /**
     * Метод для заполнения матрицы
     *
     * @param mas матрица
     */
    private static void filling(int[][] mas) {
        for (int i = 0; i <5; ++i) {
            for (int j = 0; j <7; ++j) {
                mas[i][j] = (int) (Math.random() * 10);
            }
        }
    }
}