package ru.eka.identif;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Этот класс позволяет в произвольной программе на языке java найти все идентификаторы и
 * вывести их в одну строку без коментариев и лишних пробелов.
 *
 * @author Куцкая Э.А., 15ОИТ18
 */
public class Identifier {
    public static void main(String[] args) throws IOException {
        String s;
        Pattern pattern = Pattern.compile(".*");
        Matcher matcher = pattern.matcher("");
        Pattern id = Pattern.compile("[A-Za-z_]+\\w*");
        Matcher idMatcher = id.matcher("");
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\ru\\eka\\identif\\Delete.java"));
             BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src\\ru\\eka\\identif\\Delete2.java"))) {
            while ((s = bufferedReader.readLine()) != null) {
                matcher.reset(s);
                while (matcher.find()) {
                    s = matcher.group().replaceAll("^\\s*\\/?\\*[^\\/]*[[*]-[/]]?$", "");
                    s = s.replaceAll("[\\s]{2,}", "");
                    s = s.replaceAll("Delete", "Delete2");
                    bufferedWriter.write(s);
                }
            }
        }
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src\\ru\\eka\\identif\\id.txt"));
             BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\ru\\eka\\identif\\Delete2.java"))) {
            while ((s = bufferedReader.readLine()) != null) {
                idMatcher.reset(s);
                while (idMatcher.find()) {
                    bufferedWriter.write(idMatcher.group() + "\n");
                }
            }
        }
    }
}


